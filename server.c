#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>

#include <signal.h>

#define PORT 5555
#define MAX_MSG_LENGTH 1024

#ifndef MSG_NOSIGNAL
#define MSG_NOSIGNAL SO_NOSIGPIPE
#endif

int main()
{
	int sock;
	struct sockaddr_in name;
	char buf[MAX_MSG_LENGTH] = {0};

	sock = socket(AF_INET, SOCK_STREAM, 0);
	if (sock < 0)
		printf("Error related to opening socket.");

	int optval = 1;
	setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof optval);

	name.sin_family = AF_INET;
	name.sin_addr.s_addr = INADDR_ANY;
	name.sin_port = htons(PORT);
	if (bind(sock, (void *)&name, sizeof(name)))
		printf("Error related to binding tcp socket");
	if (listen(sock, 1) == -1)
		printf("Error related to socket listening");

	struct sockaddr cli_addr;
	int cli_len = sizeof(cli_addr);
	int new_socket, new_fd, pid;
	FILE *new_stream;

	if (new_fd = dup(STDERR_FILENO) == -1)
		printf("Error related to dup function usage.");
	new_stream = fdopen(new_fd, "w");
	setbuf(new_stream, NULL);

	printf("Initializing server...\n");
	while (new_socket = accept(sock, &cli_addr, &cli_len))
	{
		printf("Client connected.\nForking... ");
		if (pid = fork())
			printf("child pid = %d.\n", pid);
		else
		{
			pid = getpid();
			if (new_socket < 0)
				printf("Error with accept function on a socket.");
			if (dup2(new_socket, STDOUT_FILENO) == -1)
				printf("Error related to dup function usage.");
			if (dup2(new_socket, STDERR_FILENO) == -1)
				printf("Error related to dup function usage.");
			while (1)
			{
				int readc = 0, filled = 0;
				while (1)
				{
					readc = recv(new_socket, buf + filled, MAX_MSG_LENGTH - filled - 1, 0);
					if (!readc)
						break;
					filled += readc;
					if (buf[filled - 1] == '\0')
						break;
				}
				if (!readc)
				{
					printf("\t[%d] Client disconnected.\n", pid);
					break;
				}
				printf("\t[%d] Command received: %s", pid, buf);
				system(buf);
				printf("\t[%d] Finished executing command.\n", pid);
				send(new_socket, "> ", 3, MSG_NOSIGNAL);
			}
			close(new_socket);
			printf("\t[%d] id process is getting killed.", pid);
			exit(0);
		}
	}
	fclose(new_stream);
	close(sock);
	return 0;
}