#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>

#include <signal.h>

#define PORT 5555
#define MAX_MSG_LENGTH 1024

#ifndef MSG_NOSIGNAL
#define MSG_NOSIGNAL SO_NOSIGPIPE
#endif

void sendCommand(int sock, int pid)
{
	char str[MAX_MSG_LENGTH] = {0};
	while (fgets(str, MAX_MSG_LENGTH, stdin) == str)
	{
		if (send(sock, str, strlen(str) + 1, 0) < 0)
			printf("Error related to send function.");
	}
	kill(pid, SIGKILL);
}

void awaitServerResponse(int sock)
{
	char buffer[MAX_MSG_LENGTH] = {0};
	int filled = 0;
	while (filled = recv(sock, buffer, MAX_MSG_LENGTH - 1, 0))
	{
		buffer[filled] = '\0';
		printf("%s", buffer);
		fflush(stdout);
	}
	printf("Connection to the server was lost.\n");
}

int main(int argc, char **argv)
{
	if (argc != 2)
		printf("error related to args.");

	int sock = socket(AF_INET, SOCK_STREAM, 0);
	if (sock == -1)
		printf("error related to socket.");

	struct in_addr server_addr;
	if (!inet_aton(argv[1], &server_addr))
		printf("Error related to inet_aton function.");

	struct sockaddr_in connection;
	connection.sin_family = AF_INET;
	memcpy(&connection.sin_addr, &server_addr, sizeof(server_addr));
	connection.sin_port = htons(PORT);
	if (connect(sock, (const struct sockaddr *)&connection, sizeof(connection)) != 0)
		printf("Error related to connect function.");

	int pid;
	if (pid = fork())
		sendCommand(sock, pid);
	else
		awaitServerResponse(sock);

	return 0;
}